# Markov Chains and Monte Carlo Algorithms

Repository to storage the codes of the discipline Markov Chains and Monte Carlo Algorithms.


<details><summary>Animação da estimativa de pi</summary>
![Teste](https://media.giphy.com/media/87PHkLMXr3XHAWhbkh/giphy.gif)
</details>


<!-- <img src="https://media.giphy.com/media/87PHkLMXr3XHAWhbkh/giphy.gif" width="450" height="200" > -->


<details><summary>Estimando *e*</summary>
![image info](./graphics/grafico_questao_5.png)
</details>

<details><summary>Quantos domínios possíveis existem na ufrj com tamanho 4 ou menos?</summary>
![image info](./graphics/grafico_questao_7.png)
</details>

<details><summary>Aproximando X~Binomial(1000, 0.2)</summary>
Aproximando com g(X) = 1

![image info](./graphics/grafico_questao_8_1.png)

Aproximando com g~Normal(np, sqrt(np(1-p)))

![image info](./graphics/grafico_questao_8_2.png)
</details>

<details><summary>Estimando integral de 0 a 10 de e^{-x^2}</summary>
![image info](./graphics/grafico_questao_9.png)
</details>


